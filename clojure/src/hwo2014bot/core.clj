(ns hwo2014bot.core
  (:require [clojure.data.json :as json])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]])
  (:gen-class))


(def throttle (atom 0.3))
(def HOST "webber.helloworldopen.com")
(def PORT "8091")
(def BOTKEY "jv/eo1Z2Z5Nd5g")
(def BOTNAME "lambda")

(defn get-result-time [msg]
  (:millis  (:result  (first  (:results  (:data msg))))))

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))
        (System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

(defmulti handle-msg :msgType)

(defmethod handle-msg "carPositions" [msg]
  {:msgType "throttle" :data @throttle})

(defmethod handle-msg :default [msg]
  {:msgType "ping" :data "ping"})

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (println "Joined")
    "gameStart" (println "Race started")
    "crash" (println "Someone crashed")
    "gameEnd" (do (println "Race ended")
                  (spit "results.txt" (str @throttle " " (get-result-time msg) "\n") :append true))
    "error" (println (str "ERROR: " (:data msg)))
    :noop))

(defn game-loop [channel]
  (let [msg (read-message channel)]
    (log-msg msg)
    (send-message channel (handle-msg msg))
    (recur channel)))

(defn -main[& [host port botname botkey]]
  (loop []
    (try
      (let [channel (connect-client-channel host (Integer/parseInt port))]
        (send-message channel {:msgType "join" :data {:name botname :key botkey}})
        (game-loop channel))
      (swap! throttle (fn [t] (+ t 0.0005)))
      (Thread/sleep 1000)
      (catch Exception e))
    (recur)))
